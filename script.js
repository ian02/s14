// alert ("Hello B146");
// alert ("Hello Again");
// alert ("Last Hello");
// console.log("Hello from the console");

//Variables are containers of data
// let name = "Zuitt";
// console.log(name);
// name = "coding bootcamp"; //no let because we just need to change the value of first let; to add new variable, declare let again
// console.log(name);

// let score;
// score = 34;
// console.log(score)

// Variables and Data Types
// let name = "Zuitt"; //string
// let name2 = 'Coding Bootcamp';
// let score = 34; //number
// let score2 = "17";
// console.log (name2);

// let name3 = "I'm feeling good today"
// console.log(name3)

// let feeling = "He said \"Hey! stop it! I'm okay now\"";
// console.log(feeling);

// console.log(name2);
// console.log(name+" "+name2);
// const legalAge = 18;
// legalAge = 16



//Functions

// function printStars (){
// 	console.log('*');
// 	console.log('**');
// 	console.log('***');
// 	console.log('****');
// }

// printStars ();
// printStars ();
// printStars ();
// printStars ();


function sayHello (){
	console.log("Hello");	
}

// Function with 1 parameter - additional recipe for your function to work properly or to add functionality

function sayHello2(x){
	console.log ("Hello " + x)
}

// sayHello2 ("Zuitt");
// sayHello2("Pinky");

//Function with 2 parameters

function sayHello3 (x,y){
	console.log ("Hello " + x + " " + y)
}

sayHello3 ("Bruce", "Wayne")

//Create a function called addNum
//that accepts two numbers and
//prints in the console the sum

function sum (x,y){
	console.log (x + y)
}

sum (1,2)





